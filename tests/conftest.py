from pathlib import Path, PureWindowsPath
import pytest
import logging
import pandas as pd
from dotenv import load_dotenv
from sys import platform


base_dir = Path(__file__).parent.parent
if platform == "win32":
    datasets_dir = base_dir / PureWindowsPath("data/test/data1.csv")
else:
    datasets_dir = base_dir / "data" / "test" / "data1.csv"
# logging configuration
logs_dir = Path(__file__).parent / "logs"


@pytest.fixture(autouse=True, name="loger")
def configure_logging(request, caplog):
    """ Set log file name same as test name, and set log level. You could change the format here too """
    exec_log = logging.getLogger('algo')
    log_file = logs_dir / ("%s.log" % request.node.name)
    request.config.pluginmanager.get_plugin("logging-plugin").set_log_path(log_file)
    caplog.set_level(logging.INFO)

    yield exec_log


@pytest.fixture(autouse=True)
def load_env():
    load_dotenv()


@pytest.fixture(autouse=True)
def dataset():
    df=pd.read_csv(datasets_dir)
    return df