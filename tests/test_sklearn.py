import numpy as np
import mlflow
import mlflow.sklearn
import os


def test_poly_fit(loger, dataset):
    """ Evaluation protocol.
    Applies the `challenger` on the provided `dataset`, and stores the model accuracy (cv-rmse) in the results_bag
    """
    # Fit the model
    loger.info("fitting model")

    storage = os.getenv("MLFLOW_S3_ENDPOINT_URL")
    tracking_uri = os.getenv("MLFLOW_TRACKING_URI")
    run_id = os.getenv("MODEL_ID")
    mlflow.set_tracking_uri(tracking_uri)
    client = mlflow.client.MlflowClient(tracking_uri,
                                        storage)

    loaded_model = mlflow.sklearn.load_model("runs:/" + run_id + "/model")
    x = dataset.drop('target', axis=1)
    y = dataset['target']
    loaded_model.fit(x, y)


    # Use the model to perform predictions
    loger.info("predicting")
    predictions = loaded_model.predict(x)

    # Evaluate the prediction error
    loger.info("evaluating error")
    cvrmse = np.sqrt(np.mean((predictions-y)**2)) / np.mean(y)
    print("Relative error (cv-rmse) is: %.2f%%" % (cvrmse * 100))

    assert cvrmse < 0.5